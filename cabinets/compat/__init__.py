from __future__ import unicode_literals

import mayan

MAYAN_VERSION = tuple(map(int, mayan.__version__.split('.')))
